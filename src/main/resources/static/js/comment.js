async function addComment(form){
    let data = new FormData(form);

    await fetch('http://localhost:3333/theme/add/comment',{
        method: 'POST',
        body: data
    }).then(r => r.json()).then(data => {
        console.log(data);
    })
    window.location.href = "/theme/" + data.get("theme_id");
}

$(document).ready(function(){
    $("#myBtn").click(function(){
        if(document.getElementById("user_status").value === "anonymousUser"){
            window.location.href = "/login"
        }
        else{
            $("#myModal").modal();
        }
    });
});