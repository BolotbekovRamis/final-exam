use `cafe_critic`;

CREATE TABLE `comments` (
                          `id` INT auto_increment NOT NULL,
                          `text` varchar(255) NOT NULL,
                          `customer_id` INT NOT NULL,
                          `cafe_id` INT NOT NULL,
                          `comment_ldt` DATETIME NOT NULL,
                          PRIMARY KEY (`id`),
                          CONSTRAINT `fk_comment_user` FOREIGN KEY (`customer_id`) REFERENCES `customers`(`id`),
                          CONSTRAINT `fk_comment_theme` FOREIGN KEY (`cafe_id`) REFERENCES `cafe` (`id`)

);