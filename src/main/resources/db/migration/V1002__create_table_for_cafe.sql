use `cafe_critic`;

CREATE TABLE `cafe` (
                         `id` INT auto_increment NOT NULL,
                         `title` varchar(225) NOT NULL,
                         `description` varchar(500) NOT NULL,
                         `customer_id` INT NOT NULL,
                         `cafe_ldt` DATETIME NOT NULL,
                         PRIMARY KEY (`id`),
                         CONSTRAINT `fk_theme_user` FOREIGN KEY (`customer_id`) REFERENCES `customers`(`id`)
);