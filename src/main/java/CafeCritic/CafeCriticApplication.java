package CafeCritic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CafeCriticApplication {

    public static void main(String[] args) {
        SpringApplication.run(CafeCriticApplication.class, args);
    }

}
