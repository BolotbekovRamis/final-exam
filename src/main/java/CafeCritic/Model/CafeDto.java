package CafeCritic.Model;

import lombok.Builder;
import lombok.Data;

import java.time.format.DateTimeFormatter;

@Data
@Builder
public class CafeDto {
    private Integer id;

    private String title;
    private String timeOfTheme;
    private String user;
    private int amountOfAnswers;
    private String description;

    public static CafeDto from(Cafe cafe) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy, HH:mm");
        return builder()
                .id(cafe.getId())
                .title(cafe.getTitle())
                .timeOfTheme(cafe.getTimeOfTheme().format(formatter))
                .user(cafe.getCustomer().getUsername())
                .amountOfAnswers(cafe.getComments().size())
                .description(cafe.getDescription())
                .build();
    }
}
