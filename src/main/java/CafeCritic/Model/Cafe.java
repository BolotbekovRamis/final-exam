package CafeCritic.Model;

import com.sun.istack.Nullable;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "cafe")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Cafe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String title;

    @Column(length = 500)
    private String description;


    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    private Customer customer;

    @Column(name = "cafe_ldt")
    @Builder.Default
    private LocalDateTime timeOfTheme = LocalDateTime.now();

    @OneToMany(mappedBy = "cafe")
    List<Comment> comments;
}
