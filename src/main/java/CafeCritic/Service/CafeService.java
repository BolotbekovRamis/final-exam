package CafeCritic.Service;

import CafeCritic.Model.Cafe;
import CafeCritic.Model.CafeDto;
import CafeCritic.Model.Comment;
import CafeCritic.Repository.CafeRepository;
import CafeCritic.Repository.CommentRepository;
import CafeCritic.Repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;


@Service
@AllArgsConstructor
public class CafeService {
    private final CafeRepository cafeRepository;
    private final CustomerRepository customerRepository;
    private final CommentRepository commentRepository;


    public void savePlace(Authentication authentication, String text, String name){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        var user = customerRepository.findByEmail(userDetails.getUsername());
        var place = Cafe.builder()
                .title(text).customer(user)
                .description(name)
                .build();

        cafeRepository.save(place);
    }

    public void saveTheme(Cafe cafe) {
        cafeRepository.save(cafe);
    }

    public Page<CafeDto> getThemes(Pageable pageable) {
        return cafeRepository.findByOrderByTimeOfThemeDesc(pageable).map(CafeDto::from);
    }

    public CafeDto getThemeById(Integer themeId) {
        return CafeDto.from(cafeRepository.findById(themeId).get());
    }

    public Cafe findThemeById(Integer themeId) {
        return cafeRepository.findById(themeId).get();
    }

    public Page<CafeDto> getPostsBySearch(Pageable pageable, String search) {
        var posts = cafeRepository.findProductsByNameAndText(search, pageable);
        return posts.map(CafeDto::from);
    }

    public void newComment(String text, Authentication authentication, int id){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        var user = customerRepository.findByEmail(userDetails.getUsername());
        var comment = Comment.builder().customer(user).timeOfComment(LocalDateTime.now()).cafe(cafeRepository.findById(id).get()).text(text).build();
        commentRepository.save(comment);

    }
}