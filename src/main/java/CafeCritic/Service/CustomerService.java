package CafeCritic.Service;

import CafeCritic.DTO.CustomerResponseDTO;
import CafeCritic.Exception.CustomerAlreadyRegisteredException;
import CafeCritic.Exception.CustomerNotFoundException;
import CafeCritic.Model.Customer;
import CafeCritic.Model.CustomerRegisterForm;
import CafeCritic.Repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CustomerService {

    private final CustomerRepository repository;
    private final PasswordEncoder encoder;
    private final CustomerRepository customerRepository;


    public CustomerResponseDTO register(CustomerRegisterForm form) {
        if (repository.existsByEmail(form.getEmail())) {
            throw new CustomerAlreadyRegisteredException();
        }

        var user = Customer.builder()
                .email(form.getEmail())
                .username(form.getUsername())
                .password(encoder.encode(form.getPassword()))
                .build();

        repository.save(user);

        return CustomerResponseDTO.from(user);
    }

    public CustomerResponseDTO getByEmail(String email) {
        var user = repository.findByEmail(email);

        return CustomerResponseDTO.from(user);
    }

    public boolean existByUsername(CustomerRegisterForm form){
        return customerRepository.existsByUsername(form.getUsername());
    }

    public Customer findByUsername(String username){
        return customerRepository.findByUsername(username);
    }
}



