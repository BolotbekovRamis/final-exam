package CafeCritic.Service;

import CafeCritic.Model.Comment;
import CafeCritic.Model.CommentDto;
import CafeCritic.Repository.CommentRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CommentService {
    private final CommentRepository commentRepository;

    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public Page<CommentDto> getComments(Integer cafe_id, Pageable pageable){
        return commentRepository.findAllByCafeIdOrderByTimeOfCommentAsc(cafe_id, pageable).map(CommentDto::from);
    }

    public void saveComment(Comment comment){
        commentRepository.save(comment);
    }
}