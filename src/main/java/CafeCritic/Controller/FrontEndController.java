package CafeCritic.Controller;

import CafeCritic.Service.CafeService;
import CafeCritic.Service.CommentService;
import CafeCritic.Service.PageService;
import CafeCritic.Service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;

@Controller
@AllArgsConstructor
public class FrontEndController {

    private final CustomerService customerService;
    private final CafeService cafeService;
    private final PageService pageService;
    private final CommentService commentService;

    @GetMapping("/")
    public String getMainPage(Model model, Pageable pageable, HttpServletRequest uriBuilder){
        var themes = cafeService.getThemes(pageable);
        var uri = uriBuilder.getRequestURI();
        constructPageable(themes, pageService.getDefaultPageSize(), model, uri);
        model.addAttribute("user_status", getUserStatus());
        return "index";
    }

    @GetMapping("/search/{search}")
    public String search(@PathVariable("search") String search, Model model, Pageable pageable, HttpServletRequest uriBuilder, Authentication authentication){
        var posts = cafeService.getPostsBySearch(pageable, search);
        var uri = uriBuilder.getRequestURI();
        constructPageable(posts, pageService.getDefaultPageSize(), model, uri);
        return "index";
    }

    @GetMapping("/theme/{cafe_id}")
    public String themePage(@PathVariable("cafe_id") Integer cafe_id, Model model, Pageable pageable,
                            HttpServletRequest uriBuilder){
        model.addAttribute("theme", cafeService.getThemeById(cafe_id));
        var comments = commentService.getComments(cafe_id, pageable);
        var uri = uriBuilder.getRequestURI();
        constructPageable(comments, pageService.getDefaultPageSize(), model, uri);
        model.addAttribute("user_status", getUserStatus());
        return "cafe";
    }

    @GetMapping("/create/theme")
    public String createTheme(Model model){
        model.addAttribute("user_status", getUserStatus());
        return "createCafe";
    }

    private String getUserStatus(){
        String user_status;
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        try{
            user_status = (String)authentication.getPrincipal();
        }
        catch (Exception ex){
            user_status = "authorizedUser";
        }
        return user_status;
    }

    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri) {
        if (list.hasNext()) {
            model.addAttribute("nextPageLink",
                    constructPageUri(uri, list.nextPageable().getPageNumber(), list.nextPageable().getPageSize()));
        }

        if (list.hasPrevious()) {
            model.addAttribute("prevPageLink",
                    constructPageUri(uri, list.previousPageable().getPageNumber(), list.previousPageable().getPageSize()));
        }

        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrev", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }

    private static String constructPageUri(String uri, int page, int size) {
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }
}
