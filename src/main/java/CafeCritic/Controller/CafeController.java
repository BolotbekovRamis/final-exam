package CafeCritic.Controller;

import CafeCritic.Model.Cafe;
import CafeCritic.Model.Comment;
import CafeCritic.Service.CafeService;
import CafeCritic.Service.CommentService;
import CafeCritic.Service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;

@Controller
@AllArgsConstructor
public class CafeController {

    private final CafeService cafeService;
    private final CustomerService customerService;
    private final CommentService commentService;


    @PostMapping("/newPlace")
    public String newPost(@RequestParam("text")String text, Authentication authentication, @RequestParam("name")String name) {

        cafeService.savePlace(authentication,text,name);
        return "redirect:/";
    }

    @PostMapping("/theme/add/comment")
    public void addComment(@RequestParam("theme_id") Integer cafe_id, @RequestParam("text") String text){
        Comment comment = new Comment();
        comment.setCustomer(customerService.findByUsername(getUsername()));
        comment.setText(text);
        comment.setCafe(cafeService.findThemeById(cafe_id));
        commentService.saveComment(comment);
    }

    @PostMapping("/newComment")
    public String newComment(@RequestParam("text") String text, @RequestParam("id")int id, Authentication authentication) {
        cafeService.newComment(text, authentication, id);
        return "redirect:/theme/" + id;
    }

    @PostMapping("/create/theme")
    public String createTheme(@RequestParam("title") String title, @RequestParam("main_text") String text){
        Cafe theme = new Cafe();
        theme.setTitle(title);
        theme.setDescription(text);
        theme.setCustomer(customerService.findByUsername(getUsername()));
        theme.setTimeOfTheme(LocalDateTime.now());
        cafeService.saveTheme(theme);
        return "redirect:/";
    }

    private String getUsername(){
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }
}
