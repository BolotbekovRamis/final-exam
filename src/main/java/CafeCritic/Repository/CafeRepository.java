package CafeCritic.Repository;

import CafeCritic.Model.Cafe;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CafeRepository extends JpaRepository<Cafe, Integer> {
    Page<Cafe> findByOrderByTimeOfThemeDesc(Pageable pageable);

    @Query("SELECT p FROM Cafe p WHERE (p.title like concat(:name, '%')) or (p.title like concat('%',:name,'%')) or (p.title like concat('%', :name))or (p.description like concat(:name, '%')) or (p.description like concat('%',:name, '%') ) or (p.description like concat('%', :name)) ")
    Page<Cafe> findProductsByNameAndText(String name, Pageable pageable);
}