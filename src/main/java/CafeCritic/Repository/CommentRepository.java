package CafeCritic.Repository;

import CafeCritic.Model.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CommentRepository extends JpaRepository<Comment, Integer> {
    Page<Comment> findAllByCafeIdOrderByTimeOfCommentAsc(Integer cafeId, Pageable pageable);
}
